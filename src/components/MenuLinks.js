import React from 'react'

import { Link } from 'react-router-dom';

export const MenuLinks = () => (
  <ul>
    <li><Link to="/"><a>Main</a></Link></li>
    <li><Link to="/online-savings-account"><a>Online Savings Account</a></Link></li>
    <li><Link to="/interest-checking-account"><a>Interest Checking Account</a></Link></li>
    <li><Link to="/money-market-account"><a>Money Market Account</a></Link></li>
    <li><Link to="/high-yield-cd"><a>High Yield CD</a></Link></li>

  </ul>
);