/* eslint-ignore react/jsx-key */
import styled from 'styled-components';
import React, { useState, useCallback } from 'react';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { Link } from 'react-router-dom'
const BannerNav = ({ content = {} }) => (
	<div className="banner-navigation">
		<ul className="left-links-list">
			{content.linksLeft && content.linksLeft.map(link => (
				<li key={`banner-nav-left-${link.linkText}`}>
					<a href={link.destination}>{link.linkText || <img src={link.linkImage.fields.file.url} alt={link.linkImage.fields.file.url} />}</a>
				</li>
			))}
		</ul>
		<ul className="right-links-list">
			{content.linksRight && content.linksRight.map(link => (
				<li key={`banner-nav-right-${link.linkText}`}>
					<a href={link.destination || '#'}>{link.linkText}</a>
				</li>
			))}
		</ul>
	</div>
);

const PageNav = ({ content = {} }) => {
	const { navigation } = content;
	const [isOpen, setIsOpen] = useState(false);
	const onNavMenuClick = useCallback(() => setIsOpen(!isOpen), [setIsOpen, isOpen]);
	return (
		<div className="page-navigation">
			<ul>
				{navigation.map(nav => {
					return (
						<li key={nav.title}>
							<div className="page-subnavigation-header" onClick={onNavMenuClick}>{nav.title}<KeyboardArrowDownIcon /></div>
							<div className={`page-subnavigation${isOpen ? '' : ' hidden'}`}>
								<ul>
									{nav.items.map((item) => {
										return (
											<li key={item.linkText}>
												<Link to={item.destination}>{item.linkText}</Link>
											</li>
										)
									})}
								</ul>
							</div>
						</li>);
				})}
			</ul>
		</div>
	);
}

const UnstyledHeader = ({ content = {}, className }) => {
	return (
		<div className={className}>
			<BannerNav content={content} />
			<PageNav content={content} />
		</div>);
}

export const Header = styled(UnstyledHeader)`
font-family: "Lato", "HelveticaNeue-Regular", "HelveticaNeue Regular", "Helvetica Neue", "Helvetica", Arial, "Lucida Grande", sans-serif;
display: inline-block;
width: 100%;
color: #2a2a2a;

a {
  text-decoration: none;
  color: #2a2a2a;
}
li {
  list-style-type: none;
}
.hidden {
	display: none
}
.banner-navigation {
	display: inline-block;
	border-bottom: solid 1px #dddddd;
	width: 100%;
	.right-links-list {
		float: right;
		display: flex;
		align-items: center;
		li { 
			padding: 1rem;
		
		}
	}
	.left-links-list {
		float: left;
		display: flex;
		align-items: center;
	}
	
}
.page-navigation {
	svg {
		width: 1rem;
		margin-left: 4px;
		height: auto;
	}
	ul {
		width: 100%;
		margin: 0;
		padding:0;

	}
	.page-subnavigation-header {
		padding: 1rem;
		width: 10rem;
		&:hover {
			cursor: pointer;
		}
	}
	.page-subnavigation {
    width: 100%;
		position: absolute;
		background-color: #f2f2f2;
		padding-bottom: 1rem;
		border-top: solid 1px #bbbbbb;
		
		z-index: 99;
		ul {
			margin-left: 2.5rem;
			margin-top: 1rem;
			margin-bottom: 1rem;
		}
		li {
			padding-top: .5rem;
			a {
				color: #0071c4;
			}
		}
	}
}


`;