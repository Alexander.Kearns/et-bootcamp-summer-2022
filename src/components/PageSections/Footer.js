/* eslint-ignore react/jsx-key */
import React from 'react'
import styled from 'styled-components';

const UnstyledFooter = ({ content, className }) => {
	return (
		<div className={className}>
			<ul>
				{content.items.map((item) => <li key={item.text}>{item.text}</li>)}
			</ul>
		</div>
	);
};

export const Footer = styled(UnstyledFooter)`
  color: #666666;
  font-family: "Lato", "HelveticaNeue-Regular", "HelveticaNeue Regular", "Helvetica Neue", "Helvetica", Arial, "Lucida Grande", sans-serif;
  font-weight: 400;
  
  li {
    list-style-type: none;
    margin-bottom: 1.5rem;
    line-height: 1.5;
  }
`;