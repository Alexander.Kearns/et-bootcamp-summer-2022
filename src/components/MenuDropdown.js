import React from 'react'
import { DropdownMenu, muiBankTheme, MetronomeProvider } from '@ally/metronome-ui';

const MenuDropdown = () => (
  <MetronomeProvider theme={muiBankTheme}>
    <DropdownMenu>
      <DropdownMenu.Trigger content={<span style={{ color: 'blue' }}>Pages</span>} />
      <DropdownMenu.MenuContainer>
        <DropdownMenu.MenuItemLink to="/" content="Main" />
        <DropdownMenu.MenuItemLink to="/interest-checking-account" content="Interest Checking Account" />
        <DropdownMenu.MenuItemLink to="/money-market-account" content="Money Market Account" />
        <DropdownMenu.MenuItemLink to="/high-yield-cd" content="High-Yield CD" />
        <DropdownMenu.MenuItemLink to="/online-savings-account" content="Online Savings Account" />
      </DropdownMenu.MenuContainer>
    </DropdownMenu>
  </MetronomeProvider >
);

export default MenuDropdown;
