import React, { useState, useEffect } from 'react';
import { PageSections } from '../components/PageSections';
import { fetchContentHighYieldCD } from '../services/content';
import { flattenPageContent } from "../utils/content";

export const HighYieldCD = () => {
  const [content, setContent] = useState({})
  useEffect(() => {
    (async () => setContent(flattenPageContent(await fetchContentHighYieldCD())))()
  }, [setContent])

  return (
    <div>
      <PageSections content={content} />
    </div>
  );
}
